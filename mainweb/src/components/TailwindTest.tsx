import React from 'react';

const TailwindTest = () => {
    return (
        <div className='p-5'>
            {/*  hover , focus , active ,bg color , border radius & padding , padding right & left  */}
            <div>
                <button className="bg-sky-500 hover:bg-sky-700  active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-300 rounded-3xl p-1.5 px-4">
                    Save changes
                </button>
            </div>

            <br /><hr />
            {/*  first and last child style differenciate */}
            <ul role="list" className="p-6 divide-y divide-slate-200">
                <li className="flex py-4 first:pt-0 last:pb-0">
                    <div className="ml-3 overflow-hidden">
                        <p className="text-sm font-medium text-slate-900">mohamad</p>
                        <p className="text-sm text-slate-500 truncate">nikparvar</p>
                    </div>
                </li>
            </ul>

            <br /><hr />
            {/* odd and even style differenciate */}
            <table>
                <tbody>
                    <tr className="odd:bg-white even:bg-slate-50">
                        <td>mo</td>
                        <td>nik</td>
                        <td>porr</td>
                    </tr>
                    <tr className="odd:bg-white even:bg-slate-500">
                        <td>mo</td>
                        <td>nik</td>
                        <td>porr</td>
                    </tr>
                </tbody>
            </table>

            <br /><hr />

            {/* required , invalid & disable options in a form*/}
            <form>
                <label className='block'>
                    <span className="py-2  block text-sm font-medium text-slate-700">Email</span>
                    <input type="email" className="mt-1 block  px-3 py-2 bg-white border border-slate-300 rounded-md text-sm shadow-sm placeholder-slate-400
                      focus:outline-none focus:border-sky-500 focus:ring-1 focus:ring-sky-500
                        disabled:bg-slate-50 disabled:text-slate-500 disabled:border-slate-200 disabled:shadow-none
                        invalid:border-pink-500 invalid:text-pink-600
                        focus:invalid:border-pink-500 focus:invalid:ring-pink-500"></input>
                </label>
            </form>

            <br /><hr />

            {/*  Group modifiers  */}
            <a href="#" className="group block max-w-xs mx-auto rounded-lg p-6 bg-white ring-1 ring-slate-900/5 shadow-lg space-y-3 hover:bg-sky-500 hover:ring-sky-500">
                <div className="flex items-center space-x-3">
                    <h3 className="text-slate-900 group-hover:text-white text-sm font-semibold">New project</h3>
                </div>
                <p className="text-slate-500 group-hover:text-white text-sm">Create a new project from a variety of starting templates.</p>
            </a>

            <br /><hr />

            {/* Styling based on sibling state (peer-{modifier}) */}
            <form>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">Email</span>
                    <input type="email" className="peer mt-1 block  px-3 py-2 bg-white border border-slate-300 rounded-md text-sm shadow-sm placeholder-slate-400
                      focus:outline-none focus:border-sky-500 focus:ring-1 focus:ring-sky-500
                        disabled:bg-slate-50 disabled:text-slate-500 disabled:border-slate-200 disabled:shadow-none
                        invalid:border-pink-500 invalid:text-pink-600
                        focus:invalid:border-pink-500 focus:invalid:ring-pink-500" />
                    <p className="mt-2 invisible peer-invalid:visible text-pink-600 text-sm">
                        Please provide a valid email address.
                    </p>
                </label>
            </form>

            <br /><hr />

            {/* different peers */}
            <fieldset>
                <legend>Published status</legend>

                <input id="draft" className="peer/draft" type="radio" name="status" checked />
                <label className="peer-checked/draft:text-sky-500">Draft</label>

                <input id="published" className="peer/published" type="radio" name="status" />
                <label className="peer-checked/published:text-sky-500">Published</label>

                <div className="hidden peer-checked/draft:block">Drafts are only visible to administrators.</div>
                <div className="hidden peer-checked/published:block">Your post will be publicly visible on your site.</div>

            </fieldset>

            <br /><hr />
            {/*  specify a word */}
            <blockquote className="text-2xl font-semibold italic text-center text-slate-900">
                When you look
                <span className="m-4 p-2 before:block before:absolute before:-inset-1 before:-skew-y-3 before:bg-pink-500 relative inline-block">
                    <span className=" relative text-white">annoyed</span>
                </span>
                all the time, people think that youre busy.
            </blockquote>
            <br /><hr />
            {/* selecting a text*/}
            <div className="selection:bg-fuchsia-300 selection:text-fuchsia-900">
                <p>
                    So I started to walk into the water. I wont lie to you boys, I was
                    terrified. But I pressed on, and as I made my way past the breakers
                    a strange calm came over me. I dont know if it was divine intervention
                    or the kinship of all living things but I tell you Jerry at that moment,
                    I <em>was</em> a marine biologist.
                </p>
            </div>
        </div>
    );
};

export default TailwindTest;